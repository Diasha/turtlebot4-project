import rclpy
from rclpy.node import Node 
from sensor_msgs.msg import Image
from cv_bridge import CvBridge 
from geometry_msgs.msg import Float64
import cv2
import numpy as np
from example_interfaces.msg import Float64

class ImageSubscriber(Node):

    def __init__(self, name):
        super().__init__(name)
        topic_name= 'video_frames'
        self.sub = self.create_subscription (
            Image, 'video_frames', self.listener_callback,10)
        self.cv_bridge = CvBridge()
        
        self.publisher_ = self.create_publisher(Float64, "location",10)
        self.timer = self.create_timer(0.1, self.publish_loaction)
        
    def listener_callback(self, data):
        self.get_logger().info('Receiving video frame')
        image = self.cv_bridge.imgmsg_to_cv2(data, 'bgr8')
        self.balls_detect(image)
  
    def balls_detect(self, image):
        hsv_img = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        lower_green = np.array([50,100,100])
        upper_green = np.array({70,255,255})
        mask1_green = cv2.InRange(hsv_img, lower_green, upper_green)
        #mask2_pink = cv2.InRange(hsv_img, lower_pink, upper_pink)
        contours, hierarchy = cv2.findContours(
            mask1_green,  cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
        for contours in contours:
            area = cv2.contourArea(contour)
            if area > 100:
                (x,y), radius = cv2.minEnclosingCircle(contour)
                center = (int(x), int(y))
                radius = int(radius)
                (cv_image, center, radius, (0, 255, 0), 2)
                cv2.circle(cv_image, center, 5, (0, 0, 255), -1)
                print("Green ball coordinates: ({}, {})".format(x, y))
                msg = Float64
                msg.data = x
                msg.data = y
                self.publisher_.publish(msg)
        cv2.imshow("balls", image)
        cv2.waitKey(10)
        
   	
def main(args=None):
    rclpy.init(args=args)
    node =ImageSubscriber()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()



