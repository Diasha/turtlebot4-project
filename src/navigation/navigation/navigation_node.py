from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from rclpy.node import Node 
from example_interfaces.msg import Float64
from nav2_simple_commander.robot_navigator import BasicNavigator, TaskResult
import rclpy
from rclpy.duration import Duration

"""
Basic navigation demo to go to pose.
"""
class Navigation(Node):
    def __init__(self):
        super().__init__('navigation_node')
        self.publisher_ = self.create_publisher(Point, 'location',10)
        self.timer = self.create_timer(0.1, self.publish_coordinates)
        self.subscriber = self.create_subscription(Point, 'location', self.location_callback, 10)
        self.x_number= 2.6601507663726807
        self.y_number= -1.084865927696228

    def publish_coordinates(self):
        msg = Point()
        msg.x= self.x_number
        msg.y = self.y_number
        self.publisher_.publish(msg)
        self.get_logger().info('Publishing: %f and %f' % (self.x_number, self.y_number))

    def location_callback(self,msg):
        self.get_logger().info('Hey Diasha! I received: X= %f and Y = %f'% (msg.x, msg.y))
        navigator = BasicNavigator()
        navigator.waitUntilNav2Active()
        goal_pose = PoseStamped()
        goal_pose.header.frame_id = 'map'
        goal_pose.header.stamp = navigator.get_clock().now().to_msg()
        goal_pose.pose.position.x = msg.x
        goal_pose.pose.position.y = msg.y
        goal_pose.pose.orientation.w = 1.0

        navigator.goToPose(goal_pose)
        i=0
        while not navigator.isTaskComplete():
            i =i +1
            feedback = navigator.getFeedback()
            if feedback and i % 5 ==0:
                print('Estimated time of arrival:'
                      +'{0: .0f}'.format(
                          Duration.from_msg(feedback.estimated_time_remaining).nanoseconds
                          / 1e9
                      )
                      + 'seconds.'
                      )
                if Duration.from_msg(feedback.navigation_time) > Duration(seconds=600.0):
                    navigator.cancelTask()
                if Duration.from_msg(feedback.navigation_time) > Duration(seconds=18.0):
                    goal_pose.pose.position.x = -3.0
                    navigator.goToPose(goal_pose)
        result = navigator.getResult()
        if result == TaskResult.SUCCEEDED: 
            print('Goal succeeded!')
        elif result == TaskResult.CANCELED:
            print('Goal was canceled!')
        elif result == TaskResult.FAILED:
            print('Goal failed!')
        else:
            print('Goal has an invalid return status!')
        navigator.lifecycleShutdown()

def main(args=None):
    rclpy.init(args=args)
    node = Navigation()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()
if __name__ == '__main__':
    main()
    
'''
def location_position():
    
    #print("Hello Diasha!")

    navigator = BasicNavigator()

    #initial_pose.pose.position.x = 3.45
    #initial_pose.pose.position.y = 2.15
    #initial_pose.pose.orientation.z = 1.0
    #initial_pose.pose.orientation.w = 0.0
    #navigator.setInitialPose(initial_pose)

    navigator.waitUntilNav2Active()

    # Go to our demos first goal pose
    goal_pose = PoseStamped()
    goal_pose.header.frame_id = 'map'
    goal_pose.header.stamp = navigator.get_clock().now().to_msg()
    goal_pose.pose.position.x = 2.6601507663726807
    goal_pose.pose.position.y = -1.084865927696228
    goal_pose.pose.orientation.w = 1.0

    # sanity check a valid path exists
    # path = navigator.getPath(initial_pose, goal_pose)

    navigator.goToPose(goal_pose)

    i = 0
    while not navigator.isTaskComplete():

        i = i + 1
        feedback = navigator.getFeedback()
        if feedback and i % 5 == 0:
            print(
                'Estimated time of arrival: '
                + '{0:.0f}'.format(
                    Duration.from_msg(feedback.estimated_time_remaining).nanoseconds
                    / 1e9
                )
                + ' seconds.'
            )

            # Some navigation timeout to demo cancellation
            if Duration.from_msg(feedback.navigation_time) > Duration(seconds=600.0):
                navigator.cancelTask()

            # Some navigation request change to demo preemption
            if Duration.from_msg(feedback.navigation_time) > Duration(seconds=18.0):
                goal_pose.pose.position.x = -3.0
                navigator.goToPose(goal_pose)

    # Do something depending on the return code
    result = navigator.getResult()
    if result == TaskResult.SUCCEEDED:
        print('Goal succeeded!')
    elif result == TaskResult.CANCELED:
        print('Goal was canceled!')
    elif result == TaskResult.FAILED:
        print('Goal failed!')
    else:
        print('Goal has an invalid return status!')

    navigator.lifecycleShutdown()
    location_position()
    exit(0)
    
    def location_pub(self):
        position_msg = Float64()
        position_msg.x = self.position_x
        position_msg.y = self.position_y
        self.publisher_.publish(position_msg)
        self.get_logger.info(f"Published Position: X={position_msg.x}, Y={position_msg.y}")
'''